package chatclient;

public class ChatException extends Throwable {
    ChatErrorResponse error;

    public ChatException(ChatErrorResponse error) {
        this.error = error;
    }

    public ChatErrorResponse getError() {
        return error;
    }
}
