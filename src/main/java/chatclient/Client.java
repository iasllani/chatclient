package chatclient;

import chatclient.config.MessageTaskScheduler;
import chatclient.dto.message.MessageListTransport;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.ScheduledFuture;


public class Client {

    private int token;
    private long lastFetched;
    private boolean inARoom;

    public void startClient() {
        try {
            authenticateClient();
            listenToRequests();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void authenticateClient() throws IOException {
        Scanner sc = new Scanner(System.in);
        handleUserAuthentication(sc);
    }

    private void requestToAuthenticate(String username, String password) {
        try {
            token = RequestHandler.handleLogin(username, password).getToken();
        } catch (ChatException e) {
            System.out.println(e.getError().getMessage());
        }
    }

    private void requestToRegister(String username, String password) {
        try {
            System.out.println(RequestHandler.handleRegistration(username, password).getResponse());
        } catch (ChatException e) {
            System.out.println(e.getError().getMessage());
        }
    }

    private void listenToRequests() throws IOException {
        Scanner sc = new Scanner(System.in);
        while (true) {
            printCommands();
            String selectedCommand = sc.nextLine();
            if (selectedCommand.matches("^[1-3]+"))
                handleCommand(Integer.parseInt(selectedCommand), sc);
        }
    }

    private void handleCommand(int command, Scanner sc) throws IOException {
        switch (command) {
            case 1:
                handleRoomCreation(sc);
                break;
            case 2:
                handleRoomJoin(sc);
                break;
            case 3:
                requestToList();
                break;
        }
    }

    private void handleUserAuthentication(Scanner sc) throws IOException {
        while (token == 0) {
            printAuthenticationCommands();
            String selectedCommand = sc.nextLine();
            if (selectedCommand.matches("^[1-2]+"))
                prepareRequest(sc, selectedCommand);
        }
    }

    private void prepareRequest(Scanner sc, String command) throws IOException {
        System.out.println("Username:");
        String username = sc.nextLine().trim();
        if (!username.matches("^[a-zA-Z0-9]+")) {
            System.out.println("Username can only contain numbers and letters");
        }
        System.out.println("Password:");
        String password = sc.nextLine().trim();
        if (!username.matches("^[a-zA-Z0-9]+")) {
            System.out.println("Password can only contain numbers and letters");
        }
        if (Integer.parseInt(command) == 1)
            requestToAuthenticate(username, password);
        else
            requestToRegister(username, password);
    }

    private void handleRoomCreation(Scanner sc) throws IOException {
        System.out.println("Please enter the name of the room you want to create: ");
        String roomName = sc.nextLine();
        while (!roomName.matches("^[a-zA-Z0-9]+")) {
            System.out.println("Room name can contain letters or numbers only.");
            System.out.println("Please enter the name of the room you want to create: ");
            roomName = sc.nextLine();
        }
        requestToCreate(roomName);
    }

    private void requestToCreate(String roomName) throws IOException {
        try {
            RequestHandler.handleRoomCreation(roomName, token);
        } catch (ChatException e) {
            System.out.println(e.getError().getMessage());
        }
    }

    private void handleRoomJoin(Scanner sc) throws IOException {
        System.out.println("Please enter the name of the room you want to join: ");
        String roomName = sc.nextLine();
        while (!roomName.matches("^[a-zA-Z0-9]+")) {
            System.out.println("Room name can contain letters or numbers only.");
            roomName = sc.nextLine();
        }
        requestToJoin(roomName);
    }

    private void requestToJoin(String roomName) throws IOException {
        RestTemplate restTemplate = new RestTemplate();
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setBearerAuth(token + "");
            ResponseEntity<MessageListTransport> response = restTemplate.exchange("http://localhost:8080/rooms/join/" + roomName, HttpMethod.GET, new HttpEntity<>(headers), MessageListTransport.class);
            lastFetched = System.currentTimeMillis();
            inARoom = true;
            System.out.println("\nSuccessfully joined room " + roomName);
            System.out.println("You are now chatting in " + roomName + ". To leave type \'-leave\'.");
            System.out.println("Messages: ");
            response.getBody().printMessages();
            startChatting();
        } catch (HttpClientErrorException ex) {
            ChatErrorResponse error = new ObjectMapper().readValue(ex.getResponseBodyAsString(), ChatErrorResponse.class);
            System.out.println("\n" + error.getMessage() + "\n");
        }
    }

    private void requestToList() {
        try {
            RequestHandler.handleListRooms(token);
        } catch (ChatException e) {
            System.out.println(e.getError().getMessage());
        }
    }

    private void startChatting() throws IOException {
        onShutdownLeaveRoom();
        ScheduledFuture scheduledFuture = startPolling();
        Scanner sc = new Scanner(System.in);
        String content = sc.nextLine().trim();
        while (!content.equals("-leave")) {
            sendMessage(content);
            content = sc.nextLine();
        }
        stopPolling(scheduledFuture);
        inARoom = false;
    }

    private void stopPolling(ScheduledFuture scheduledFuture) throws IOException {
        requestToLeave();
        scheduledFuture.cancel(true);
    }

    private void sendMessage(String content) {
        try {
            RequestHandler.handleSendMessage(token, content);
        } catch (ChatException e) {
            System.out.println(e.getError().getMessage());
        }
    }

    private ScheduledFuture startPolling() {
        return MessageTaskScheduler.getTaskScheduler().scheduleAtFixedRate(this::checkUnread, 10000);
    }

    private void checkUnread() {
        RestTemplate restTemplate = new RestTemplate();
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setBearerAuth(token + "");
            ResponseEntity<MessageListTransport> response = restTemplate.exchange("http://localhost:8080/messages/unread", HttpMethod.POST, new HttpEntity<>(lastFetched, headers), MessageListTransport.class);
            lastFetched = System.currentTimeMillis();
            response.getBody().printMessages();
        } catch (HttpClientErrorException ex) {
            ChatErrorResponse error = null;
            try {
                error = new ObjectMapper().readValue(ex.getResponseBodyAsString(), ChatErrorResponse.class);
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("\n" + error.getMessage() + "\n");
        }
    }

    private void requestToLeave() throws IOException {
        try {
            RequestHandler.handleRoomLeave(token);
        } catch (ChatException e) {
            System.out.println(e.getError().getMessage());
        }
    }

    private void printCommands() {
        System.out.println("\nSupported commands: " +
                "\n1. Create new room" +
                "\n2. Join existing room" +
                "\n3. List all rooms" +
                "\nThe number of the command you want to do: ");
    }

    private void printAuthenticationCommands() {
        System.out.println("\nSupported commands: " +
                "\n1. Login" +
                "\n2. Register" +
                "\nThe number of the command you want to do: ");
    }

    private void onShutdownLeaveRoom() {
        Runtime.getRuntime().addShutdownHook(
                new Thread(() -> {
                    if (inARoom) {
                        try {
                            requestToLeave();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                })
        );
    }

}
