package chatclient;

import chatclient.dto.authentication.AuthenticationRequestTransport;
import chatclient.dto.authentication.AuthenticationTokenTransport;
import chatclient.dto.chatroom.ChatRoomNameTransport;
import chatclient.dto.chatroom.ChatRoomNamesListTransport;
import chatclient.dto.chatroom.ChatRoomResponseTransport;
import chatclient.dto.deletion.RoomDeletionResponseTransport;
import chatclient.dto.deletion.UserDeletionResponseTransport;
import chatclient.dto.message.MessageReceivedTransport;
import chatclient.dto.message.NewMessageTransport;
import chatclient.dto.registration.RegistrationRequestTransport;
import chatclient.dto.registration.RegistrationResponseTransport;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;

public class RequestHandler {

    public static RegistrationResponseTransport handleRegistration(String username, String password) throws ChatException {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<RegistrationResponseTransport> response = null;
        try {
            response = restTemplate.exchange("http://localhost:8080/users/register", HttpMethod.POST, new HttpEntity<>(new RegistrationRequestTransport(username, password)), RegistrationResponseTransport.class);
        } catch (HttpClientErrorException ex) {
            ChatErrorResponse error = null;
            try {
                error = new ObjectMapper().readValue(ex.getResponseBodyAsString(), ChatErrorResponse.class);
                throw new ChatException(error);
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("\n" + error.getMessage() + "\n");
        }
        return response.getBody();
    }

    public static AuthenticationTokenTransport handleLogin(String username, String password) throws ChatException {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<AuthenticationTokenTransport> response = null;
        try {
            response = restTemplate.exchange("http://localhost:8080/users/authenticate", HttpMethod.POST, new HttpEntity<>(new AuthenticationRequestTransport(username, password)), AuthenticationTokenTransport.class);
            System.out.println("\nSuccessfully authenticated " + username);
        } catch (HttpClientErrorException ex) {
            ChatErrorResponse error = null;
            try {
                error = new ObjectMapper().readValue(ex.getResponseBodyAsString(), ChatErrorResponse.class);
                throw new ChatException(error);
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("\n" + error.getMessage() + "\n");
        }
        return response.getBody();
    }

    public static UserDeletionResponseTransport handleUserDeletion(Integer token) throws ChatException {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<UserDeletionResponseTransport> response = null;
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setBearerAuth(token + "");
            response = restTemplate.exchange("http://localhost:8080/users/delete", HttpMethod.DELETE, new HttpEntity<>(headers), UserDeletionResponseTransport.class);
        } catch (HttpClientErrorException ex) {
            ChatErrorResponse error = null;
            try {
                error = new ObjectMapper().readValue(ex.getResponseBodyAsString(), ChatErrorResponse.class);
                throw new ChatException(error);
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("\n" + error.getMessage() + "\n");
        }
        return response.getBody();
    }

    public static ChatRoomNameTransport handleRoomCreation(String roomName, Integer token) throws ChatException {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<ChatRoomNameTransport> response = null;
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setBearerAuth(token + "");
            response = restTemplate.exchange("http://localhost:8080/rooms/create/" + roomName, HttpMethod.GET, new HttpEntity<>(headers), ChatRoomNameTransport.class);
            System.out.println("\nSuccessfully created room: " + response.getBody().getRoomName());
        } catch (HttpClientErrorException ex) {
            ChatErrorResponse error = null;
            try {
                error = new ObjectMapper().readValue(ex.getResponseBodyAsString(), ChatErrorResponse.class);
                throw new ChatException(error);
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("\n" + error.getMessage() + "\n");
        }
        return response.getBody();
    }

    public static RoomDeletionResponseTransport handleRoomDeletion(String roomName, Integer token) throws ChatException {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<RoomDeletionResponseTransport> response = null;
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setBearerAuth(token + "");
            response = restTemplate.exchange("http://localhost:8080/rooms/delete/" + roomName, HttpMethod.GET, new HttpEntity<>(headers), RoomDeletionResponseTransport.class);
        } catch (HttpClientErrorException ex) {
            ChatErrorResponse error = null;
            try {
                error = new ObjectMapper().readValue(ex.getResponseBodyAsString(), ChatErrorResponse.class);
                throw new ChatException(error);
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("\n" + error.getMessage() + "\n");
        }
        return response.getBody();
    }

    public static ChatRoomNamesListTransport handleListRooms(Integer token) throws ChatException {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<ChatRoomNamesListTransport> response = null;
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setBearerAuth(token + "");
            response = restTemplate.exchange("http://localhost:8080/rooms", HttpMethod.GET, new HttpEntity<>(headers), ChatRoomNamesListTransport.class);
            response.getBody().printRoomNames();
        } catch (HttpClientErrorException ex) {
            ChatErrorResponse error = null;
            try {
                error = new ObjectMapper().readValue(ex.getResponseBodyAsString(), ChatErrorResponse.class);
                throw new ChatException(error);
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("\n" + error.getMessage() + "\n");
        }
        return response.getBody();
    }

    public static ChatRoomResponseTransport handleRoomLeave(Integer token) throws ChatException {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<ChatRoomResponseTransport> response = null;
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setBearerAuth(token + "");
            response = restTemplate.exchange("http://localhost:8080/rooms/leave", HttpMethod.GET, new HttpEntity<>(headers), ChatRoomResponseTransport.class);
        } catch (HttpClientErrorException ex) {
            ChatErrorResponse error = null;
            try {
                error = new ObjectMapper().readValue(ex.getResponseBodyAsString(), ChatErrorResponse.class);
                throw new ChatException(error);
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("\n" + error.getMessage() + "\n");
        }
        return response.getBody();
    }

    public static MessageReceivedTransport handleSendMessage(Integer token, String content) throws ChatException {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<MessageReceivedTransport> response = null;
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setBearerAuth(token + "");
            response = restTemplate.exchange("http://localhost:8080/messages/send", HttpMethod.POST, new HttpEntity<>(new NewMessageTransport(content), headers), MessageReceivedTransport.class);
        } catch (HttpClientErrorException ex) {
            ChatErrorResponse error = null;
            try {
                error = new ObjectMapper().readValue(ex.getResponseBodyAsString(), ChatErrorResponse.class);
                throw new ChatException(error);
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("\n" + error.getMessage() + "\n");
        }
        return response.getBody();
    }
}
