package chatclient.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

@Configuration
public class MessageTaskScheduler {

    private static ThreadPoolTaskScheduler taskScheduler;

    @Bean
    public TaskScheduler taskScheduler() {
        taskScheduler = new ThreadPoolTaskScheduler();
        taskScheduler.setPoolSize(20);
        taskScheduler.setThreadNamePrefix("Message receiver");
        return taskScheduler;
    }

    public static TaskScheduler getTaskScheduler() {
        return taskScheduler;
    }
}
