package chatclient.dto.authentication;

public class AuthenticationTokenTransport {

    private int token;

    public AuthenticationTokenTransport() {
    }

    public AuthenticationTokenTransport(int token) {
        this.token = token;
    }

    public int getToken() {
        return token;
    }

    public void setToken(int token) {
        this.token = token;
    }
}
