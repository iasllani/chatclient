package chatclient.dto.chatroom;

public class ChatRoomNameTransport {

    private String roomName;

    public ChatRoomNameTransport() {
    }

    public ChatRoomNameTransport(String roomName) {
        this.roomName = roomName;
    }


    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }
}