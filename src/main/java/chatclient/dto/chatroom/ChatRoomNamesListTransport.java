package chatclient.dto.chatroom;


import java.util.List;

public class ChatRoomNamesListTransport {

    private List<String> names;

    public ChatRoomNamesListTransport() {
    }


    public List<String> getNames() {
        return names;
    }

    public void setNames(List<String> names) {
        this.names = names;
    }

    public void printRoomNames() {
        names.forEach(System.out::println);
    }
}
