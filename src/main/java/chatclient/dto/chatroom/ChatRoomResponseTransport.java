package chatclient.dto.chatroom;

public class ChatRoomResponseTransport {

    private String response;

    public ChatRoomResponseTransport(String response) {
        this.response = response;
    }

    public ChatRoomResponseTransport() {
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
