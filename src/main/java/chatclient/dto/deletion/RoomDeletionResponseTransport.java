package chatclient.dto.deletion;

public class RoomDeletionResponseTransport {

    private String response;

    public RoomDeletionResponseTransport() {
    }

    public RoomDeletionResponseTransport(String response) {
        this.response = response;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
