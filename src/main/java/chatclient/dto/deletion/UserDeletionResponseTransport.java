package chatclient.dto.deletion;

public class UserDeletionResponseTransport {

    private String response;

    public UserDeletionResponseTransport(String response) {
        this.response = response;
    }

    public UserDeletionResponseTransport() {
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
