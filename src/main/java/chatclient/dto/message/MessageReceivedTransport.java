package chatclient.dto.message;

public class MessageReceivedTransport {

    private String response;

    public MessageReceivedTransport() {
    }

    public MessageReceivedTransport(String response) {
        this.response = response;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
