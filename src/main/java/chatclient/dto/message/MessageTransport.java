package chatclient.dto.message;


import java.sql.Timestamp;

public class MessageTransport {

    private String senderName;

    private Timestamp timestamp;

    private String content;

    public MessageTransport() {
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }


    @Override
    public String toString() {
        return senderName + ":\n" + "[" + timestamp + "]  " + content;
    }

}
