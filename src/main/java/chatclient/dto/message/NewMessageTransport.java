package chatclient.dto.message;

import java.sql.Timestamp;

public class NewMessageTransport {

    private String content;
    private Timestamp timestamp;

    public NewMessageTransport() {
    }

    public NewMessageTransport(String content) {
        this.content = content;
        this.timestamp = new Timestamp(System.currentTimeMillis());
    }


    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }
}
