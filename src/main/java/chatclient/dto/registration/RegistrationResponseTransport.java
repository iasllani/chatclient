package chatclient.dto.registration;

public class RegistrationResponseTransport {

    private String response;

    public RegistrationResponseTransport() {
    }

    public RegistrationResponseTransport(String response) {
        this.response = response;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
