import chatclient.ChatException;
import chatclient.RequestHandler;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class UserIntegrationTests {

    @Test
    public void test_client_registration() throws ChatException {
        assertEquals(RequestHandler.handleRegistration("testing", "testing").getResponse(), "Successfully registered");
        RequestHandler.handleUserDeletion(-1422446064);
    }

    @Test
    public void test_valid_client_authentication_request() throws ChatException {
        RequestHandler.handleLogin("username", "password");
    }

    @Test(expected = ChatException.class)
    public void test_duplicate_client_registration_request() throws ChatException {
        RequestHandler.handleRegistration("username", "password").getResponse();
    }

    @Test(expected = ChatException.class)
    public void test_empty_client_username__registration_request() throws ChatException {
        RequestHandler.handleRegistration("    ", "aaaa").getResponse();
    }

    @Test(expected = ChatException.class)
    public void test_empty_client_password_registration_request() throws ChatException {
        RequestHandler.handleRegistration("aaaa", "     ").getResponse();
    }

    @Test(expected = ChatException.class)
    public void test_invalid_client_username_registration_request() throws ChatException {
        RequestHandler.handleRegistration("....", "password").getResponse();
    }

    @Test(expected = ChatException.class)
    public void test_invalid_client_password_registration_request() throws ChatException {
        RequestHandler.handleRegistration("username", "....").getResponse();
    }

    @Test(expected = ChatException.class)
    public void test_invalid_empty_username_registration_request() throws ChatException {
        RequestHandler.handleRegistration("   ", "....").getResponse();
    }

    @Test
    public void test_room_creation() throws ChatException {
        assertEquals(RequestHandler.handleRoomCreation("testing", "username".hashCode()).getRoomName(), "testing");
        RequestHandler.handleRoomDeletion("testing", "username".hashCode());
    }

}
